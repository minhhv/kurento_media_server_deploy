
const path = require('path');
const url = require('url');
const express = require('express');
const minimist = require('minimist');
const WsConnect = require("./lib/wsconnect");
var indexRouter = require('./routes/index');

const fs    = require('fs');
const https = require('https');
// const http = require('http');

var argv = minimist(process.argv.slice(2), {
    default: {
        as_uri: 'http://localhost:5000/',
		ws_uri: 'ws://172.18.169.20:8888/kurento'
        // as_uri: 'https://kms.yattare.jp:20002/',
        //  ws_uri: 'ws://localhost:40051/kurento'
        //ws_uri: 'wss://kms.yattare.jp:40052/kurento'
    }
});

let credentials = {
    key:  fs.readFileSync('./keys/key_test.key'),
    cert: fs.readFileSync('./keys/key_test.crt')
};

/*
 * Server startup
 */
var asUrl = url.parse(argv.as_uri);
var port = asUrl.port;
var server = https.createServer(credentials, app).listen(port, function() {
// var server = http.createServer(app).listen(port, function() {
    console.log('Ws Kurento started', 'Port: ' + port);
});
