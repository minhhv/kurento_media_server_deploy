var kurento = require('kurento-client');
var os = require('os');
var fs    = require('fs');
const { Console } = require("console");
const myLogger = new Console({
	stdout: fs.createWriteStream("././debug_log.txt")
});

class WsAction {

	/*
	* Definition of global variables.
	*/
	constructor(argv){
        this.candidatesQueue = {};
        this.kurentoClient = null;
        this.presenters = {};
        this.viewers = {};
        this.argv = argv;
        this._dbpr;
        this.noPresenterMessage = 'No active presenter. Try again later...';
    }

	/*
	* Definition of functions
	*/

	// Recover kurentoClient for the first time.
	getKurentoClient(callback) {
		if (this.kurentoClient !== null) {
			return callback(null, this.kurentoClient);
		}

		console.log("gooo", this.argv.ws_uri);

		kurento(this.argv.ws_uri, (error, _kurentoClient) => {console.log("goooxxxxxxxxxxxx00000000000");
			if (error) {
				
				console.log("Could not find media server at address " + this.argv.ws_uri);
				return callback("Could not find media server at address" + this.argv.ws_uri
						+ ". Exiting with error " + error);
			}
			console.log("goooxxxxxxxxxxxx");
			this.kurentoClient = _kurentoClient;
			callback(null, this.kurentoClient);
		});
	}
	calculateCPUusedPercent(){
		let cpus = os.cpus();
		let total = 0;
		cpus.forEach(cpu => {
			total += Object.values(cpu.times).reduce(
				(acc, tv) => acc + tv, 0
			);
		});
		
		// Accumulate every CPU times values
		
		// Normalize the one returned by process.cpuUsage() 
		// (microseconds VS miliseconds)
		let usage = process.cpuUsage();
		let currentCPUUsage = (usage.user + usage.system) * 1000;
		
		// Find out the percentage used for this specific CPU
		let perc = currentCPUUsage / total * 100;
		
		return perc.toFixed(2);
	}

	async startPresenter(sessionId, ws, {sdpOffer, gameid, magic, cam}, callback) {
		console.log("start", sessionId, " gameid:", gameid, "magic:", magic, " cam:", cam);
		
		this.clearCandidatesQueue(sessionId);
		// if(!this.presenters) this.presenters = {};
		// console.log("1111111111111", this.presenters);
		if(!this.presenters[sessionId]) this.presenters[sessionId] = {};
		if(!this.presenters[sessionId][gameid]) this.presenters[sessionId][gameid] = {};
		if(!this.presenters[sessionId][gameid][magic]) this.presenters[sessionId][gameid][magic] = {};

		if (this.presenters[sessionId][gameid][magic][cam] !== null && this.presenters[sessionId][gameid][magic][cam] !== undefined) {
			console.log("stop 111");
			this.stop(sessionId);
			return callback("Another user is currently acting as presenter. Try again later ...");
		}

		let presenter = {
			id : sessionId,
			webRtcEndpoint : null
		}

		// console.log("22222222222222", this.presenters);

		await this.getKurentoClient(async (error, kurentoClient) => {
			if (error) {
				console.log("stop 222");
				this.stop(sessionId);
				return callback(error);
			}

			// console.log('33333333333', this.presenters);

			if (!this.presenters[sessionId][gameid][magic].pipeline) {
				console.log('ppppppppp', this.presenters[sessionId][gameid][magic].pipeline);
				await kurentoClient.create('MediaPipeline', (error, pipeline) => {
					if (error) {
						console.log("stop 333");
						this.stop(sessionId);
						return callback(error);
					}

					console.log("prs - create pl");

					if(this.presenters[sessionId] && this.presenters[sessionId][gameid] && this.presenters[sessionId][gameid][magic]) {
						this.presenters[sessionId][gameid][magic].pipeline = pipeline;
					}
				});
			}

			// console.log("5555555555", this.presenters);

			if (!this.presenters[sessionId] || !this.presenters[sessionId][gameid] || !this.presenters[sessionId][gameid][magic] || this.presenters[sessionId][gameid][magic].pipeline === null) {
				console.log("stop 444");
				this.stop(sessionId);
				return callback(this.noPresenterMessage);
			}
				
			this.presenters[sessionId][gameid][magic].pipeline.create('WebRtcEndpoint', (error, webRtcEndpoint) => {
				if (error) {
					console.log("stop 555");
					this.stop(sessionId);
					return callback(error);
				}

				if (presenter === null) {
					console.log("stop 666");
					this.stop(sessionId);
					return callback(this.noPresenterMessage);
				}

				if (this.candidatesQueue[sessionId]) {
					while(this.candidatesQueue[sessionId].length) {
						var candidate = this.candidatesQueue[sessionId].shift();
						webRtcEndpoint.addIceCandidate(candidate);
					}
				}

				webRtcEndpoint.setMaxVideoRecvBandwidth(320);
            	webRtcEndpoint.setMinVideoRecvBandwidth(100);

				// console.log('webRtcEndpoint', webRtcEndpoint);

				webRtcEndpoint.on('OnIceCandidate', (event) => {
					var candidate = kurento.getComplexType('IceCandidate')(event.candidate);
					ws.send(JSON.stringify({
						id : 'iceCandidate',
						cam : cam,
						gameid : gameid,
						magic : magic,
						candidate : candidate
					}));
				});

				webRtcEndpoint.gatherCandidates((error) => {
					if (error) {
						console.log("stop 999");
						this.stop(sessionId);
						return callback(error);
					}
				});

				presenter.webRtcEndpoint = webRtcEndpoint;
				this.presenters[sessionId][gameid][magic][cam] = presenter;
				this.presenters[sessionId][gameid]['time'] = (new Date().getTime());
			});
			console.log('===============================');
			console.log('num of presenters: '+ Object.keys(this.presenters).length);
			console.log('num of viewers: '+  Object.keys(this.viewers).length);
			console.log('CPU used: ' + this.calculateCPUusedPercent() + '%');
			console.log('Total Mem: '+ os.totalmem());
			console.log('Free Mem: '+ os.freemem());
			console.log('Time: '+ new Date());
			kurentoClient.getServerManager().then(function(serverManager) {
				serverManager.getPipelines(function (error, pipelines) {
					console.log('num of pipelines: '+pipelines.length);
					console.log('===============================');
				});
			});
			console.log('===============================');
		});
	}

	// doublePresenter(sessionId, {gameid, magic, cam, dmagic, did}, callback) {
	// 	console.log("start db", sessionId, " gameid:", gameid, "magic:", magic, " cam:", cam);
	// 	this.clearCandidatesQueue(sessionId);
	// 	if(!this.presenters[sessionId]) this.presenters[sessionId] = {};
	// 	if(!this.presenters[sessionId][gameid]) this.presenters[sessionId][gameid] = {};
	// 	if(!this.presenters[sessionId][gameid][magic]) this.presenters[sessionId][gameid][magic] = {};

	// 	if (this.presenters[sessionId][gameid][magic][cam] !== null && this.presenters[sessionId][gameid][magic][cam] !== undefined) {
	// 		this.stop(sessionId);
	// 		return callback("Another user is currently acting as presenter. Try again later ...");
	// 	}

	// 	var _dbpri = 0;
	// 	this._dbpr = setInterval(() => {
	// 		if (this.presenters && this.presenters[sessionId] && this.presenters[sessionId][gameid]) {
	// 			let _presenter = this.presenters[sessionId][gameid][dmagic];
	// 			if (typeof _presenter !== 'undefined' && typeof _presenter[did] !== 'undefined') {
	// 				clearInterval(this._dbpr);
	// 				this.presenters[sessionId][gameid][magic][cam] = _presenter[did];
	// 				// console.log(_presenter[cam].sdpAnswer);
	// 				// return callback(null, _presenter[cam].sdpAnswer);
	// 				callback(null);
	// 			}
	// 		}

	// 		if (_dbpri > 200) {
	// 			clearInterval(this._dbpr);
	// 		}
	// 		_dbpri++;
	// 	}, 100);
	// }
}

module.exports = WsAction;