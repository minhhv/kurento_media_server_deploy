var ws = require('ws');
var WsAction = require('./action');

class WsConnect {
	constructor(server, argv){
		this.idCounter = 0;
		this.liveTime = 6000000;
        this.wss = new ws.Server({
			server : server,
			path : '/connect'
		});
		this.action = new WsAction(argv);
    }

	nextUniqueId() {
		this.idCounter++;
		return this.idCounter.toString();
	}

	pingAlive(ws) {
		ws.isAlive = Math.floor(Date.now()) + (this.liveTime * 5);
	}

	/*
	* Management of WebSocket messages
	*/
	init() {
		this.wss.on('connection', (ws) => {
			var _this = this;
			var sessionId = this.nextUniqueId();
			console.log('Connection received with sessionId ' + sessionId);

			ws.on('error', (error) => {
				console.log('Connection ' + sessionId + ' error');
				this.action.stop(sessionId);
			});

			ws.isAlive = Math.floor(Date.now()) + (this.liveTime * 5);
			ws.ssId = sessionId;

			const interval = setInterval(function ping() {
				_this.wss.clients.forEach(function each(ws) {
					if (ws.isAlive < Math.floor(Date.now())) {
						console.log("check ss~~~close"+ ws.ssId +'-'+ws.isAlive);
						return ws.terminate();
					}
					
					console.log("check ss~~~"+ws.ssId +'-'+ws.isAlive);

					ws.send(JSON.stringify({
						id : 'ping',
					}));
				});
			}, this.liveTime);

			ws.on('close', () => {
				console.log('Connection ' + sessionId + ' closed');
				this.action.stop(sessionId);
				clearInterval(interval);
			});
		});
	}
}

module.exports = WsConnect;