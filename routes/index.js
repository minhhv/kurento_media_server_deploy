var express = require('express');
var router = express.Router();

router.get('/gameview/:gameid/:idsCam/:idsCam2', function(req, res) {
    let gameid = req.params.gameid;
    let cams = req.params.idsCam;
    let cams2 = req.params.idsCam2;
    if (cams) {
        const camsArr = cams.split(",");
        const camsArr2 = cams2.split(",");
        res.render('index', {gameid, camsList: JSON.stringify(camsArr), camsList2: JSON.stringify(camsArr2)});
    }
});

module.exports = router;